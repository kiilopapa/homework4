import java.util.*;

/** Quaternions. Basic operations. */
public class Quaternion {

    private double rPart = 0.;
    private double iPart = 0.;
    private double jPart = 0.;
    private double kPart = 0.;
    private static final double e = 0.0000001;

   /** Constructor from four double values.
    * @param a real part
    * @param b imaginary part i
    * @param c imaginary part j
    * @param d imaginary part k
    */
   public Quaternion (double a, double b, double c, double d) {
       rPart = a;
       iPart = b;
       jPart = c;
       kPart = d;

   }

    public Quaternion() {

    }

    /** Real part of the quaternion.
    * @return real part
    */
   public double getRpart() {
      return rPart;
   }

   /** Imaginary part i of the quaternion. 
    * @return imaginary part i
    */
   public double getIpart() {
      return iPart;
   }

   /** Imaginary part j of the quaternion. 
    * @return imaginary part j
    */
   public double getJpart() {
      return jPart;
   }

   /** Imaginary part k of the quaternion. 
    * @return imaginary part k
    */
   public double getKpart() {
      return kPart;
   }

   /** Conversion of the quaternion to the string.
    * @return a string form of this quaternion: 
    * "a+bi+cj+dk"
    * (without any brackets)
    */
   @Override
   public String toString() {
       String r = String.valueOf(rPart);
       String i = String.valueOf(iPart) + "i";
       if (!i.startsWith("-")) i = "+" + i;
       String j = String.valueOf(jPart) + "j";
       if (!j.startsWith("-")) j = "+" + j;
       String k = String.valueOf(kPart) + "k";
       if (!k.startsWith("-")) k= "+" + k;

       return r + i + j + k;
   }

   /** Conversion from the string to the quaternion. 
    * Reverse to <code>toString</code> method.
    * @throws IllegalArgumentException if string s does not represent 
    *     a quaternion (defined by the <code>toString</code> method)
    * @param s string of form produced by the <code>toString</code> method
    * @return a quaternion represented by string s
    */
   public static Quaternion valueOf (String s) {
       char[] signs = new char[4];
       if (s.startsWith("-")) {
           signs[0] = '-';
           s = s.substring(1, s.length());
       }

       String[] parts = s.split("-|[+]");

       if (!Integer.valueOf(parts.length).equals(4)) throw new IllegalArgumentException( "User input: " + s + "<-- Wrong number of parts!");
       if (!parts[1].endsWith("i")) throw new IllegalArgumentException( "User input: " + s + "<-- Imaginary part i missing or @ wrong place");
       if (!parts[2].endsWith("j")) throw new IllegalArgumentException( "User input: " + s + "<-- Imaginary part j missing or @ wrong place");
       if (!parts[3].endsWith("k")) throw new IllegalArgumentException( "User input: " + s + "<-- Imaginary part k missing or @ wrong place");

       int count = 1;
       for (int i = 1; i < s.length(); i++) {
           if (s.charAt(i)=='-' || s.charAt(i)=='+'){
               signs[count] = s.charAt(i);
               count++;
           }
       }
       if (signs[0] == '-') parts[0] = '-' + parts[0];
       for (int i = 1; i <4; i++) {
           parts[i] = parts[i].substring(0, parts[i].length()-1);
           if (signs[i] == '-') parts[i] = '-' + parts[i];
           //System.out.println(parts[i]);
       }

       try {
           Double a = Double.parseDouble(parts[0]);
           Double b = Double.parseDouble(parts[1]);
           Double c = Double.parseDouble(parts[2]);
           Double d = Double.parseDouble(parts[3]);

           return new Quaternion(a, b, c, d);
       }catch (NumberFormatException e){
           throw new IllegalArgumentException("Invalid input!");
       }
   }

   /** Clone of the quaternion.
    * @return independent clone of <code>this</code>
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
      return new Quaternion(rPart, iPart, jPart, kPart);
   }

   /** Test whether the quaternion is zero. 
    * @return true, if the real part and all the imaginary parts are (close to) zero
    */
   public boolean isZero() {

       return Math.abs(rPart)<e && Math.abs(iPart)<e && Math.abs(jPart)<e && Math.abs(kPart)<e;
       //return Double.valueOf(rPart).equals(0.) && Double.valueOf(iPart).equals(0.)
       //       && Double.valueOf(jPart).equals(0.) && Double.valueOf(kPart).equals(0.);
   }

   /** Conjugate of the quaternion. Expressed by the formula 
    *     conjugate(a+bi+cj+dk) = a-bi-cj-dk
    * @return conjugate of <code>this</code>
    */
   public Quaternion conjugate() {
      if (Math.abs(iPart)<e && Math.abs(jPart)<e && Math.abs(kPart)<e)
          return new Quaternion(rPart, iPart, jPart, kPart);
      return new Quaternion(rPart, -iPart, -jPart, -kPart);
   }

   /** Opposite of the quaternion. Expressed by the formula 
    *    opposite(a+bi+cj+dk) = -a-bi-cj-dk
    * @return quaternion <code>-this</code>
    */
   public Quaternion opposite() {
       if (this.isZero()) return this;
      return new Quaternion(-rPart, -iPart, -jPart, -kPart);
   }

   /** Sum of quaternions. Expressed by the formula 
    *    (a1+b1i+c1j+d1k) + (a2+b2i+c2j+d2k) = (a1+a2) + (b1+b2)i + (c1+c2)j + (d1+d2)k
    * @param q addend
    * @return quaternion <code>this+q</code>
    */
   public Quaternion plus (Quaternion q) {
      return new Quaternion(rPart+q.rPart, iPart+q.iPart, jPart+q.jPart, kPart+q.kPart);
   }

   /** Product of quaternions. Expressed by the formula
    *  (a1+b1i+c1j+d1k) * (a2+b2i+c2j+d2k) = (a1a2-b1b2-c1c2-d1d2) + (a1b2+b1a2+c1d2-d1c2)i +
    *  (a1c2-b1d2+c1a2+d1b2)j + (a1d2+b1c2-c1b2+d1a2)k
    * @param q factor
    * @return quaternion <code>this*q</code>
    */
   public Quaternion times (Quaternion q) {
       double a1 = rPart;
       double b1 = iPart;
       double c1 = jPart;
       double d1 = kPart;
       double a2 = q.rPart;
       double b2 = q.iPart;
       double c2 = q.jPart;
       double d2 = q.kPart;
       double r = a1*a2-b1*b2-c1*c2-d1*d2;
       double i = a1*b2+b1*a2+c1*d2-d1*c2;
       double j = a1*c2-b1*d2+c1*a2+d1*b2;
       double k = a1*d2+b1*c2-c1*b2+d1*a2;

       return new Quaternion( r, i, j, k);
   }

   /** Multiplication by a coefficient.
    * @param r coefficient
    * @return quaternion <code>this*r</code>
    */
   public Quaternion times (double r) {
       return new Quaternion(rPart*r, iPart*r, jPart*r, kPart*r);
   }

   /** Inverse of the quaternion. Expressed by the formula
    *     1/(a+bi+cj+dk) = a/(a*a+b*b+c*c+d*d) + 
    *     ((-b)/(a*a+b*b+c*c+d*d))i + ((-c)/(a*a+b*b+c*c+d*d))j + ((-d)/(a*a+b*b+c*c+d*d))k
    * @return quaternion <code>1/this</code>
    */
   public Quaternion inverse() {
       if (this.isZero())throw new IllegalArgumentException("division by zero not allowed");
       double a = rPart;
       double b = iPart;
       double c = jPart;
       double d = kPart;
       if ((a*a+b*b+c*c+d*d)<e)throw new IllegalArgumentException("division by zero not allowed");
       double r = a/(a*a+b*b+c*c+d*d);
       double i = (-b)/(a*a+b*b+c*c+d*d);
       double j = (-c)/(a*a+b*b+c*c+d*d);
       double k = (-d)/(a*a+b*b+c*c+d*d);
       return new Quaternion(r, i, j, k);
   }

   /** Difference of quaternions. Expressed as addition to the opposite.
    * @param q subtrahend
    * @return quaternion <code>this-q</code>
    */
   public Quaternion minus (Quaternion q) {
      return this.plus(q.opposite());
   }

   /** Right quotient of quaternions. Expressed as multiplication to the inverse.
    * @param q (right) divisor
    * @return quaternion <code>this*inverse(q)</code>
    */
   public Quaternion divideByRight (Quaternion q) {
       if (q.isZero())throw new IllegalArgumentException("division by zero not allowed");
       double a = q.rPart;
       double b = q.iPart;
       double c = q.jPart;
       double d = q.kPart;
       if ((a*a+b*b+c*c+d*d)<e)throw new IllegalArgumentException("division by zero not allowed");
       return this.times(q.inverse());
   }

   /** Left quotient of quaternions.
    * @param q (left) divisor
    * @return quaternion <code>inverse(q)*this</code>
    */
   public Quaternion divideByLeft (Quaternion q) {
      if (this.isZero())throw new IllegalArgumentException("division by zero not allowed");
       double a = this.rPart;
       double b = this.iPart;
       double c = this.jPart;
       double d = this.kPart;
       if ((a*a+b*b+c*c+d*d)<e)throw new IllegalArgumentException("division by zero not allowed");
      return q.inverse().times(this);
   }
   
   /** Equality test of quaternions. Difference of equal numbers
    *     is (close to) zero.
    * @param qo second quaternion
    * @return logical value of the expression <code>this.equals(qo)</code>
    */
   @Override
   public boolean equals (Object qo) {
       try {
           Quaternion q = (Quaternion) qo;
           return Math.abs(rPart-q.rPart)<e && Math.abs(iPart-q.iPart)<e
                   && Math.abs(jPart-q.jPart)<e && Math.abs(kPart-q.kPart)<e;
           /*return Double.valueOf(rPart).equals(q.rPart) && Double.valueOf(iPart).equals(q.iPart)
                   && Double.valueOf(jPart).equals(q.jPart) && Double.valueOf(kPart).equals(q.kPart);
                   */
       }catch (ClassCastException e) {
           return false;
       }
   }

   /** Dot product of quaternions. (p*conjugate(q) + q*conjugate(p))/2
    * @param q factor
    * @return dot product of this and q
    */
   public Quaternion dotMult (Quaternion q) {
      return this.times(q.conjugate()).plus(q.times(this.conjugate())).times(0.5);
   }

   /** Integer hashCode has to be the same for equal objects.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      return this.toString().hashCode();
   }

   /** Norm of the quaternion. Expressed by the formula 
    *     norm(a+bi+cj+dk) = Math.sqrt(a*a+b*b+c*c+d*d)
    * @return norm of <code>this</code> (norm is a real number)
    */
   public double norm() {
      return Math.sqrt(rPart*rPart+iPart*iPart+jPart*jPart+kPart*kPart);
   }

   /** Main method for testing purposes. 
    * @param arg command line parameters
    */
   public static void main (String[] arg) {
       //System.out.println(Quaternion.valueOf("blabla"));
      Quaternion arv1 = new Quaternion (-1., 1, 2., -2.);
       //Quaternion hh = arv1.conjugate();
       //System.out.println(hh.toString());
       //Quaternion h = new Quaternion();
       //System.out.println(hh.plus(arv1));
       //System.out.println("tühi "+h.isZero());
      //Quaternion q = Quaternion.valueOf("-1.0+1.0i+2.0j+-2.0k");
       //System.out.println(q);
      if (arg.length > 0)
         arv1 = valueOf (arg[0]);
      System.out.println ("first: " + arv1.toString());
      System.out.println ("real: " + arv1.getRpart());
      System.out.println ("imagi: " + arv1.getIpart());
      System.out.println ("imagj: " + arv1.getJpart());
      System.out.println ("imagk: " + arv1.getKpart());
      System.out.println ("isZero: " + arv1.isZero());
      System.out.println ("conjugate: " + arv1.conjugate());
      System.out.println ("opposite: " + arv1.opposite());
      System.out.println ("hashCode: " + arv1.hashCode());
      Quaternion res = null;
      try {
         res = (Quaternion)arv1.clone();
      } catch (CloneNotSupportedException e) {};
      System.out.println ("clone equals to original: " + res.equals (arv1));
      System.out.println ("clone is not the same object: " + (res!=arv1));
      System.out.println ("hashCode: " + res.hashCode());
      res = valueOf (arv1.toString());
      System.out.println ("string conversion equals to original: " 
         + res.equals (arv1));
      Quaternion arv2 = new Quaternion (1., -2.,  -1., 2.);
      if (arg.length > 1)
         arv2 = valueOf (arg[1]);
      System.out.println ("second: " + arv2.toString());
      System.out.println ("hashCode: " + arv2.hashCode());
      System.out.println ("equals: " + arv1.equals (arv2));
      res = arv1.plus (arv2);
      System.out.println ("plus: " + res);
      System.out.println ("times: " + arv1.times (arv2));
      System.out.println ("minus: " + arv1.minus (arv2));
      double mm = arv1.norm();
      System.out.println ("norm: " + mm);
      System.out.println ("inverse: " + arv1.inverse());
      System.out.println ("divideByRight: " + arv1.divideByRight (arv2));
      System.out.println ("divideByLeft: " + arv1.divideByLeft (arv2));
      System.out.println ("dotMult: " + arv1.dotMult (arv2));
   }
}
// end of file
